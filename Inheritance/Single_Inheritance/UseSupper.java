class A
{
	int i;
	void Show()
	{
		System.out.println("i in SupperClass: " + i);
	}
}
class B extends A
{
	int i; // overrides i in class A
	B(int a, int b)
	{
		super.i = a; // i in class A
		i = b;      // i in class B
	}
	void Show()   // overides show() Method of supper class
	{
		//show(); // Incorrect . will call recursively to Show() method of sub class
		super.Show(); // calls show() of A class
		System.out.println("i in SubClass: " + i);
	}
}
public class UseSupper 
{
		public static void main(String arg[])
		{
			B obj = new B(10,20);
			obj.Show();
			
		}

}
