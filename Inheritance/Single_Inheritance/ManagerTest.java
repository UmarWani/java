import java.time.*;
class Employee
{
	private String name;
	private double salary;
	private LocalDate hireDay;
	
	Employee(String name, double salary, int day, int month, int year)
	{
		this.name = name;
		this.salary = salary;
		hireDay = LocalDate.of(year, month, day);
	}
	String getName()
	{
		return name;
	}
	double getSalary()
	{
		return salary;
	}
	LocalDate getHireDay()
	{
		return hireDay;
	}
	void raiseSalary(int byPercent)
	{
		double raise = (byPercent / 100) * salary;
		salary += salary + raise;
	}
}
class Manager extends Employee
{
	private double bonus;
	
	Manager(String name, double salary, int year, int month, int day)
	{
		super(name,salary,year,month,day);
		bonus = 0;
	}
	double getSalary()
	{
		return bonus + super.getSalary();
	}
	void setBonus(double b)
	{
		bonus = b;
	}
}
public class ManagerTest
{
	public static void main(String arg[])
	{
	Manager boss = new Manager("umar",10000,7,11,1993);
	boss.setBonus(500);
	
	Employee[] staff = new Employee[3];
	staff[0] = boss;
	staff[1] = new Employee("Aadil",10000,7,11,1993);
	staff[2] = new Employee("Requya",10000,7,11,1993);
	
	for (Employee e : staff)
		 System.out.println("name = " + e.getName() + " HireDay = " + e.getHireDay() + " Salary = " + e.getSalary());
	}
}
