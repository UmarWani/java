class BaseClass
{
	int number1;
	int number2;
	
	void setValue(int number1, int number2)
	{
		this.number1 = number1;
		this.number2 = number2;
		
	}
	void show()
	{
		System.out.println("Number1 and Number2 are : " + number1 + "  " + number2);
	}
}
class SubClass extends BaseClass
{
	int result;
	
	void sum()
	{
		result = number1 + number2; 
	}
	void showResult()
	{
		System.out.println("result is : " + result);
	}
}
public class SimpleInheritance 
{
		public static void main(String arg[])
		{
			BaseClass superObj = new BaseClass();
			superObj.setValue(10,20);
			System.out.println("Contents of superObj are:");
			superObj.show();
			
			SubClass subObj = new SubClass();
			subObj.setValue(100, 200);
			System.out.println("\nContents of subObj are:");
			subObj.show();
			subObj.showResult();
			
			
		}

}
