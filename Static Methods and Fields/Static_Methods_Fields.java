
class Employeee 
{
	static String Department = "P.H.E";
	String name;
	int salary;
	
		Employeee(String name, int salary)
		{
			this.name = name;
			this.salary = salary;
		}
		
		String getName()
		{
			return name;
		}
		
		int getsalary()
		{
			return salary;
		}
		static String getDept()
		{
			
			return Department;
		}
}

public class Static_Methods
{
	public static void main(String arg[])
	{
		Employeee staff[] = new Employeee[3];
		
		staff[0] = new Employeee("umar",1000);
		staff[1] = new Employeee("Aadil",2000);
		staff[2] = new Employeee("Requya",3000);
		
		System.out.println("Name\tDept\tSalary");
		
		for(Employeee e : staff)
		{
		   System.out.println(e.getName() + "\t" + Employeee.getDept() + "\t" + e.getsalary());
	
		}
	}
		
}
