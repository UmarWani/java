// Using break to exit a loop.

public class Break_Demo1 
{
	public static void main(String arg[])
	{
		for(int i=0; i < 10; i++)
		{
			if(i==7) break;
			System.out.print(i + "\t");
		}
	}

}
