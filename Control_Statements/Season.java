
import java.util.Scanner;
public class Season 
{
	public static void main(String arg[])
	{
		Scanner input = new Scanner(System.in);
		int month;
		String season;
		
		System.out.println("Enter month Number\t");
		month = input.nextInt();
		
		if(month == 12  || month == 1 || month == 2) 
			season = "Winter";
		else if(month == 3 || month == 4 || month == 5) 
			season = "Spring";
		else if(month == 6 || month == 7 || month == 8)
			season = "Summer";
		else if(month == 9 || month == 10 || month == 11)
			season = "Autumn";
		else
			season = "NULL";
		
		if(season == "NULL")
			System.out.print("Invalid Month Number");
		else
			System.out.print("Month no. " + month + " is in " + season);
	}

}
