
public class Average 
{
	public static void main(String arg[])
	{
		int[] Numbers = {1,2,3,4,5,6,7,8,9,10};
		int sum = 0;
		
		System.out.print("values are : \t");
		for(int i : Numbers)  // Extended for loop
		{
			System.out.print(i + "\t");
			sum = sum + i;
		}
		
		System.out.print("\nAverage is:-\t" + sum / 10);
	}

}
