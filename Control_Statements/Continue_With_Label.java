
public class Continue_With_Label 
{
	public static void main(String arg[])
	{
		label: for(int i = 0; i < 10; i++)
		{
			for(int j = 0; j < 10; j++)
			{
				if(j > i)
				{
					System.out.println();
					continue label;
				}
				System.out.print(j + " ");
			}
		}
	}

}
