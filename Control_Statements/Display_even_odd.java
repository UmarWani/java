import java.util.Scanner;
public class Display_even_odd 
{
	public static void main(String arg[])
	{
		int Upper_Limit;
		Scanner input = new Scanner(System.in);
		
		System.out.print("enter the upper limit to which u want to display Even Odd Numbers:-\t");
		Upper_Limit = input.nextInt();
		
		System.out.println("EVEN \tODD");
		
		for(int i = 0; i < Upper_Limit; i++)
		{
			System.out.print(i + "  \t");
			if(i % 2 == 0) continue;
			System.out.println();
		}	
				
	}

}
