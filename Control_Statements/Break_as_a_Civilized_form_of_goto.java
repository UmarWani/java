
// how break with Lablel is used to exit from Nested loops

public class Break_as_a_Civilized_form_of_goto 
{
	public static void main(String arg[])
	{
		boolean Flag = true;
		First:{
			Second:{
				Third:{
					System.out.println("Before the break.");
					if(Flag) break Second; // break out of second block
					System.out.println("This won't execute");
				}
			System.out.println("This won't execute");
			}
		System.out.println("This is after second block.");
		}
	}
}
