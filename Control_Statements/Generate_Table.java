// Variation of For Loop
import java.util.Scanner;

public class Generate_Table 
{
	public static void main(String arg[])
	{
		int Upper_limit, Table_of;
		Scanner input = new Scanner(System.in);
		
		System.out.println("Enter the number whose table u want to print");
		Table_of = input.nextInt();
		System.out.println("Enter the Upper Limit");
		Upper_limit = input.nextInt();
		
		int Result;
		boolean Done = false;
		
		for(int i = 1; !Done; i++)
		{
			Result = Table_of * i;
			System.out.println(Table_of + " * " + i + " = " + Result);
			if(i == Upper_limit)
				Done = true;
		}
		
	}

}
