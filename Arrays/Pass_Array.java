import java.util.Arrays;

public class Pass_Array 
{
	public static void main(String arg[])
	{
		int[] Array = {1,2,3,4,5};
		
		System.out.print("Array before modify is:- \n ");
		System.out.print(Arrays.toString(Array));
		
		modifyArray(Array);
		
		System.out.print("\nArray After modify is:- \n ");
		System.out.print(Arrays.toString(Array));
			
	}
	public static void modifyArray(int[] Array)
	{
		for(int i =0; i < Array.length; i++)
		{
			Array[i] = Array[i] + 2;
		}
	}

}
