import java.util.Arrays;

public class Array_Sort 
{
	public static void main(String arg[])
	{
		int[] Array = {11,23,3,14,5};
		
		System.out.print("Array before Sort is:- \n ");
		System.out.print(Arrays.toString(Array));
		
		Arrays.sort(Array); // inbuilt method of sorting array(insertion sort)
		
		System.out.print("\nArray After Sort is:- \n ");
		System.out.print(Arrays.toString(Array));
			
	}
	


}
