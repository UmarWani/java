class StringMethods
{
	String fName;
	String mName;
	String lName;
	
	
	StringMethods(String fName, String mName, String lName)
	{
	 this.fName = fName;
	 this.mName = mName;
	 this.lName = lName;
	}

	void StringConcatenation()
	{
		System.out.println("OutPut of StringConcatenation Method is :");
		System.out.println("\tName is " + " " + fName + " " + mName + " " + lName);
	}
	void StringComparision()
	{
		Boolean result;
		result = fName.equals(mName);

		System.out.println("OutPut of StringComparision Method is :");
		if(result == true)
		   System.out.println("\tfName and mName are Same");
		else
		   System.out.println("\tfName and mName are not  Same");
	}
	
	void StringLengthMethod()
	{
		System.out.println("OutPut of StringLengthMethod is :");
		System.out.println("\tString   Length");
		System.out.println("\t" + fName + "     " + fName.length());	
		System.out.println("\t" + mName + "     " + mName.length());
		System.out.println("\t" + lName + "     " + lName.length());
	}
	
	void SubStringMethod()
	{
		System.out.println("OutPut of SubStringMethod is :");
		System.out.println("\tString    Ist 3 charcters");
		System.out.println("\t" + fName + "      " + fName.substring(0,3));
		System.out.println("\t" + mName + "   " + mName.substring(0,3));
		System.out.println("\t" + lName + "      " + lName.substring(0,3));
		
	}
	void stringUpperCaseMethod()
	{
		System.out.println("OutPut of StringUpperCaseMethod is :");
		System.out.println("\tLowerCase     UpperCase");
		System.out.println("\t" + fName + "\t" + fName.toUpperCase());
		System.out.println("\t" + mName + "\t" + mName.toUpperCase());
		System.out.println("\t" + lName + "\t" + lName.toUpperCase());
	}
}

public class StringMethodsTest
{
	public static void main(String arg[])
	{
		StringMethods student = new StringMethods("umar","Mushtaq","Wani");	
		
		student.StringConcatenation();
		student.StringComparision();
		student.StringLengthMethod();
		student.SubStringMethod();
		student.stringUpperCaseMethod();
	}

}