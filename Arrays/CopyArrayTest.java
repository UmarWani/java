import java.util.Arrays;

class CopyingArrayVaraible
{
	int x =10;
	public int[] smallPrimes = {2,3,5,6,7,9};
	 public int[] luckyNumbers;
	
	 void copy()
	{
		 luckyNumbers = smallPrimes;
		
		display();
	}
	private void display()
	{
		
		 
		 System.out.println("After Copying Array Varaible, both Array Varaibles refer to same Array");
		System.out.println("Elemnts of smallPrimes Array are : \t " + Arrays.toString(smallPrimes));
		System.out.print("Elemnts of luckyNumbers Array are : \t " + Arrays.toString(luckyNumbers));
	}
}
class CopyArrayValues
{
	public int[] smallPrimes = {2,3,5,6,7,9};
	 public int[] luckyNumbers;
void copy()
	{
		 
		luckyNumbers = Arrays.copyOf(smallPrimes,smallPrimes.length);
		display();
	}
	private void display()
	{
		
		 
		 System.out.println("After Copying Array Varaible, both Array Varaibles refer to same Array");
		System.out.println("Elemnts of smallPrimes Array are : \t " + Arrays.toString(smallPrimes));
		System.out.print("\nElemnts of luckyNumbers Array are : \t " + Arrays.toString(luckyNumbers));
	}
}
public class CopyArrayTest
{
	public static void main(String Arg[])
	{
	
		{
		CopyingArrayVaraible arr = new CopyingArrayVaraible();
	    arr.copy();
		}
		{
		CopyArrayValues arr = new CopyArrayValues();
	    arr.copy();
		}
	}
}