import java.security.SecureRandom;

public class RollDie
{
	public static void main(String arg[])
	{
		final int roll_Die = 100;
		SecureRandom randomNumbers = new SecureRandom();
		int[] Frequency = new int[7];
		
		// roll die 100 times; use die value as frequency index
		for (int roll = 1; roll <= roll_Die; roll++)
			++Frequency[1 + randomNumbers.nextInt(6)];
		System.out.printf("%s%10s%n", "Face", "Frequency");
		
		 // output each array element's value
		 for (int face = 1; face < Frequency.length; face++)
		 System.out.printf("%4d%10d%n", face, Frequency[face]);
	}
	

}
